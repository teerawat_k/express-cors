const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");

const SELF_URL = "www.example.com";
const server = express();
const dev = process.env.NODE_ENV !== "production";
const port = 3000;
/* Express configuration. */
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

if (!dev) {
  server.use(cors({ origin: SELF_URL }));
} else {
  server.use(cors({ origin: "*" }));
}

server.get("/health-check", (req, res) => {
  return res.status(200).json({ message: "Hello CORS" });
});

server.listen(port, () =>
  console.log(`Example server listening on port ${port}!`)
);
